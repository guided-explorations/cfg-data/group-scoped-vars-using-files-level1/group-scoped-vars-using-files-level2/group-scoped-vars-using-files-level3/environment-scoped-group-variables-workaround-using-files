## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/cfg-data/group-scoped-vars-using-files-level1/group-scoped-vars-using-files-level2/group-scoped-vars-using-files-level3/environment-scoped-group-variables-workaround-using-files)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## This Is a Stop-Gap Solution
Once the following feature request is resolved, this solution will no longer be necessary and may be retired or repurposed.  Please follow this issue to know when the product can do this natively: https://gitlab.com/gitlab-org/gitlab/-/issues/2874

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-08-13

* **GitLab Version Released On**: 13.2

* **GitLab Edition Required**: 

  * For overall solution: [![FC](../config-data-monorepo/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **References and Featured In**:


## Demonstrates These Design Requirements and Desirements

As a complete whole, this guided exploration requires at least the Free / Core (![FC](images/FC.png)) edition of GitLab.

- **GitLab Development Pattern:** the ability to have environment scoped variables at a group level that act similar to project scoped variables in the following ways:
  - Are converged down the group heirarchy from top to bottom with...
  - Precedence given to lower (closer) levels to the project using them
  - Are optional (do not have to exist for a specific level or environment)
  - Can be associated to named environment.

## Using This Pattern:
- This pattern requires that the user used to trigger the job has read access to the "Group Config Vars In Files" (group-config-vars-in-files) projects at all upbound groups levels.
